﻿var consts = require('./game/scripts/app/consts.js');
var objects = require('./game/scripts/app/Objects.js');

var nodeStatic = require('node-static');
var fileServer = new nodeStatic.Server('./game');
var fs = require('fs');
var app = require('http').createServer(
    (req, res) => {
        req.addListener('end', () => {
            fileServer.serve(req, res);
        }).resume();
    });

export var io = require('socket.io').listen(app);

app.listen(3000, () => {
    console.log('listening on *:3000');
});


var bees = require('./game/scripts/bees.js');
bees.serverControlled = true;
bees.start();

//On the initial connection, send all data to the client
io.on('connection', (socket) => {
    console.log('a user connected, sending data');
    var list = [];
    var key;
    var obj;
    //Sends the bees
    for (key in bees.beeList) {
        var bee = bees.beeList[key];
        obj = { caste: bee.caste, id: bee.hash(), x: bee.x, y: bee.y, color: bee.color };
        list.push(obj);
    }
    //Sends the queens
    for (key in bees.queenList) {
        var queen = bees.queenList[key];
        obj = { caste: queen.caste, id: queen.hash(), x: queen.x, y: queen.y, color: queen.color };
        list.push(obj);
    }
    //Emit
    socket.emit('enter', list);

    //Sends all the food on the grid.
    var grid = bees.grid;
    list = [];
    for (var x = 0; x < bees.grid.getLength(0); x++) {
        for (var y = 0; y < bees.grid.getLength(1); y++) {
            var cell = bees.grid.getCellByIndex(x, y);
            obj = { col: cell.col, row: cell.row, food: cell.food };
            list.push(obj);
        }
    }
    //Emit
    socket.emit('grid', list);
});

