<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="./game/beestyle.css"></link>
    </head>
    <body>
        <?php include 'header.php'; ?>

        <h1>Bees!</h1>
        <div id="canvas">
            <canvas id="simulation_canvas" width="600" height="600" style="border:1px solid black"></canvas>
        </div>
        <div id="stats">
            
        </div>
        <script src="./game/Bee.js"></script>
        <script src="./game/collections.js"></script>
        <script src="./game/consts.js"></script>
        <script src="./game/bees.js"></script>
        <script>
            start(false);
        </script>
        
    </body>
</html>
