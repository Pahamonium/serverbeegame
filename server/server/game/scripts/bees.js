var obj = require('./app/Objects');
var Bee = require('./app/Bee');
var consts = require('./app/consts');
var server = require('../../server');
var io = server.io;
exports.serverControlled = false;
exports.ctx;
exports.beeList = {};
exports.queenList = {};
exports.grid;
exports.queenCounter = 1;
//FOR HTML STUFF
exports.stats;
exports.sections = {}; //A dictionary with a key (index) value.
var fpsCounter = 0;
var fpsTick = 1; //one second
var sendUpdateCounter = 0;
var sendUpdateTick = 0.5;
var updateList = [[]];
var updateListMaxLength = 300;
var sendGridDataCounter = 0;
var sendGridDataTick = 1;
var gridData = [];
//Where the game starts
function start() {
    if (!exports.serverControlled) {
        var canvas = document.getElementById("simulation_canvas");
        exports.ctx = canvas.getContext("2d");
        canvas.width = consts.canvasWidth;
        canvas.height = consts.canvasHeight;
        exports.stats = document.getElementById('stats');
    }
    //Initially calculate delta
    obj.Time.updateDelta();
    this.grid = new obj.Grid(consts.COLS, consts.ROWS);
    for (var i = 0; i < consts.STARTING_QUEENS; i++) {
        var queen = new Bee.Bee(consts.QUEEN, '#' + (Math.random().toString(16) + '000000').slice(2, 8), 0, Math.random() * exports.grid.getLength(0) * consts.SQUARE_SIZE, Math.random() * exports.grid.getLength(1) * consts.SQUARE_SIZE, consts.QUEEN_BASE_SIZE, consts.QUEEN_BASE_SPEED, 35, null, 100, consts.AGE_MAX_QUEEN);
        queen.queenRef = queen;
        this.addBee(queen);
    }
    update();
}
exports.start = start;
;
//The main game loop.
function update() {
    //Calculate delta
    obj.Time.updateDelta();
    fpsCounter += obj.Time.deltaTime;
    sendUpdateCounter += obj.Time.deltaTime;
    var sendUpdate = false;
    //Calculate FPS every second
    if (fpsCounter >= fpsTick) {
        fpsCounter -= fpsTick;
        obj.Time.setFps();
    }
    if (sendUpdateCounter >= sendUpdateTick) {
        sendUpdateCounter -= sendUpdateTick;
        sendUpdate = true;
    }
    //Redraw the canvas
    if (!exports.serverControlled)
        drawCanvas();
    updateList = updateBees(exports.beeList, sendUpdate, updateList);
    updateList = updateBees(exports.queenList, sendUpdate, updateList);
    sendGraphUpdate();
    if (sendUpdate) {
        console.log('sending ' + updateList.length + ' lists, there is ' + (Object.keys(exports.beeList).length + Object.keys(exports.queenList).length) + ' total bees, fps: ' + obj.Time.getFps());
        for (var i = 0; i < updateList.length; i++)
            io.sockets.emit('update', updateList[i]);
        updateList = [[]];
    }
    if (!exports.serverControlled)
        drawElements();
    setTimeout(function () { return update(); }, 30);
}
;
//Updates a list of bees.
function updateBees(list, sendUpdate, updateList) {
    for (var key in list) {
        var currBee = list[key];
        if (currBee.health <= 0) {
            removeBee(currBee, list);
            continue;
        }
        currBee.update(obj.Time.deltaTime);
        //If not the server, do a draw call.
        if (!exports.serverControlled)
            currBee.render(obj.Time.deltaTime);
        else {
            if (updateList[updateList.length - 1].length >= updateListMaxLength) {
                updateList.push([]); //Update the list.
            }
            //Otherwise, if it is time to send an update, save the ID, X, and Y values to send.
            if (sendUpdate) {
                var data = { id: currBee.hash(), x: currBee.x, y: currBee.y, size: currBee.size };
                updateList[updateList.length - 1].push(data);
            }
        }
    }
    return updateList;
}
//Draws html elements
function drawElements() {
    for (var key in exports.sections) {
        var elem = exports.sections[key];
        if (elem.queen.health <= 0) {
            exports.stats.removeChild(elem.element);
            delete elem;
            delete exports.sections[key];
            continue;
        }
        elem.element.innerHTML = "Queen " + elem.queen.num + "<br>Health: " + (~~(elem.queen.health * 10) / 10) + "<br>Food: " + ~~elem.queen.food + "<br>ColonySize: " + elem.queen.colonySize;
    }
}
//Clears the canvas and draws the food.
function drawCanvas() {
    exports.ctx.globalAlpha = 1;
    exports.ctx.fillStyle = "#D4BBA5";
    exports.ctx.fillRect(0, 0, consts.canvasWidth, consts.canvasHeight);
    for (var i = 0; i < exports.grid.getLength(0); i++)
        for (var j = 0; j < exports.grid.getLength(1); j++) {
            var cell = this.grid.getCellByIndex(i, j);
            if (cell.food > 5) {
                exports.ctx.globalAlpha = cell.food / 100;
                exports.ctx.fillStyle = "#D60E00";
                exports.ctx.fillRect(consts.SQUARE_SIZE * i, consts.SQUARE_SIZE * j, consts.SQUARE_SIZE, consts.SQUARE_SIZE);
            }
            if (cell.needsUpdating) {
                this.gridData.push({ col: cell.col, row: cell.row, food: cell.getFood() });
                cell.needsUpdating = false;
            }
        }
    exports.ctx.globalAlpha = 1;
    exports.ctx.fillStyle = "#000000";
    exports.ctx.fillText("FPS: " + obj.Time.getFps(), consts.canvasWidth - 50, 20, 100);
}
function sendGraphUpdate() {
    //console.log('timer: ' + sendGridDataCounter+' delta: '+obj.Time.deltaTime);
    sendGridDataCounter += obj.Time.deltaTime;
    if (sendGridDataCounter >= sendGridDataTick) {
        sendGridDataCounter -= sendGridDataTick;
        gridData = [];
        exports.grid.iterate(function (cell) {
            if (cell.needsUpdating) {
                var obj = { col: cell.col, row: cell.row, food: cell.getFood() };
                gridData.push(obj);
                cell.needsUpdating = false;
            }
        });
        if (gridData.length > 0)
            io.emit('gridUpdate', gridData);
    }
}
function addSectionToHTML(queen) {
    if (!this.serverControlled) {
        var statsElement = document.getElementById('stats');
        var element = document.createElement('div');
        element.setAttribute('id', queen.hash().toString());
        element.setAttribute('class', 'stat');
        statsElement.appendChild(element);
        exports.sections[queen.hash()] = new obj.CustomElement(element, queen);
    }
}
exports.addSectionToHTML = addSectionToHTML;
//Adds a bee to the list and sends it over the network.
function addBee(bee) {
    //Get the right list.
    if (bee.caste === consts.QUEEN)
        exports.beeList[bee.hash()] = bee;
    else
        exports.queenList[bee.hash()] = bee;
    ;
    //Send some info! (caste, id, x, y, color)
    var obj = { caste: bee.caste, id: bee.hash(), x: bee.x, y: bee.y, color: bee.color };
    io.emit('create', obj);
}
exports.addBee = addBee;
//Removes a bee from the list and sends the action over the network.
function removeBee(bee, list) {
    bee.die();
    //Send some info (caste, id)
    var obj = { caste: bee.caste, id: bee.hash() };
    io.emit('delete', obj);
    var remove = list[bee.hash()];
    delete list[bee.hash()];
    delete remove;
}
//# sourceMappingURL=bees.js.map