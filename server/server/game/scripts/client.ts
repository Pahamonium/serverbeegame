///<reference path="./app/consts.ts"/>

//We have to delcare 'io' to be able to use it. Typescript doesn't know what 'io' is but we reference it
//in the index.html
declare var io;

class SimpleCell {
    public food = 0;

    constructor(public col, public row) {

    }
}

class SimpleGrid {
    public gridArray = new Array();
    public rows;
    public cols;
    constructor(width, height, public squareSize) {
        this.cols = ~~(width / squareSize + 1);
        this.rows = ~~(height / squareSize + 1);
        for (var col = 0; col < this.cols; col++) {
            this.gridArray[col] = new Array();
            for (var row = 0; row < this.rows; row++) {
                this.gridArray[col][row] = new SimpleCell(col, row);
            }
        }
    }

    getCell(x, y): SimpleCell {
        var col = ~~(x / this.squareSize);
        var row = ~~(y / this.squareSize);
        return this.gridArray[col][row];
    }

    getCellByIndex(xIndex, yIndex): SimpleCell {
        return this.gridArray[xIndex][yIndex];
    }

    iterate(func) {
        for (var col = 0; col < this.gridArray.length; col++) {
            for (var row = 0; row < this.gridArray.length; row++) {
                func(this.gridArray[col][row]);
            }
        }
    }

}

console.log("Connected to server");
var socket = io.connect();

var ctx: CanvasRenderingContext2D = (<HTMLCanvasElement>(document.getElementById('simulation_canvas'))).getContext("2d");

var beeList: { [index: number]: SimpleBee } = {};
var queenList: { [index: number]: SimpleBee } = {};

var interpolate = true;

var grid = new SimpleGrid(600, 600, 25);

socket.on('update', (list) => {
    var beeData, tmp;

    for (var i = 0; i < list.length; i++) {
        beeData = list[i]; //Cache the current object in the list

        if (beeData.caste === 3)
            tmp = queenList[beeData.id];
        else
            tmp = beeList[beeData.id];

        if (tmp) {
            //tmp.x = beeData.x;
            //tmp.y = beeData.y;
            tmp.updatePosition(beeData.x, beeData.y);
            tmp.size = beeData.size;
        }
    }
});

socket.on('enter', (list) => {
    for (var i = 0; i < list.length; i++) {
        var bee = list[i];
        if (bee.caste !== 3)
            this.beeList[bee.id] = new SimpleBee(bee.x, bee.y, bee.id, bee.color);
        else {
            this.queenList[bee.id] = new SimpleBee(bee.x, bee.y, bee.id, bee.color);
        }
    }
});

socket.on('grid', (list) => {
    var data, cell;
    for (var i = 0; i < list.length; i++) {
        data = list[i];
        cell = this.grid.getCellByIndex(data.col, data.row);
        cell.food = data.food;
    }
});

socket.on('gridUpdate', (list) => {
    var data;
    for (var i = 0; i < list.length; i++) {
        data = list[i];
        this.grid.getCellByIndex(data.col, data.row).food = data.food;
    }
});

    socket.on('create', (bee) => {
    if (bee.caste === 3) {
        queenList[bee.id] = new SimpleBee(bee.x, bee.y, bee.id, bee.color);
    } else {
        beeList[bee.id] = new SimpleBee(bee.x, bee.y, bee.id, bee.color);
    }
});

socket.on('delete', (bee) => {
    var remove;
    console.log('removedBee');
    if (bee.caste === 3) {
        remove = queenList[bee.id];
        delete queenList[bee.id];
        delete remove;
    } else {
        remove = beeList[bee.id];
        delete beeList[bee.id];
        delete remove;
    }
});


class Time {
    public static lastTime = 0;
    public static currTime = 0;
    public static deltaTime = 0;
    private static fps = 0;

    static calcDelta() {
        this.lastTime = this.currTime;
        this.currTime = new Date().getTime();
        this.deltaTime = (this.currTime - this.lastTime) / 1000;
    }

    static getFps(): number {
        return this.fps;
    }

    static calcFps() {
        this.fps = ~~(1 / this.deltaTime);
    }
}

var fpsCounter = 0, fpsTick = 1;

function lerp(a: number, b: number, t: number) : number{
    return a + t * (b - a);
}

(function start() {
    Time.calcDelta();
    draw();
})();

function draw() {
    ctx.fillStyle = '#A8A58D';
    ctx.fillRect(0, 0, 600, 600);

    Time.calcDelta();

    fpsCounter += Time.deltaTime;
    if (fpsCounter >= fpsTick) {
        fpsCounter -= fpsTick;
        Time.calcFps();
    }

    ctx.fillStyle = '#E34C2D';
    this.grid.iterate((cell: SimpleCell) => {
        if (cell.food > 0) {
            ctx.globalAlpha = cell.food / 100;
            ctx.fillRect(cell.col*25, cell.row*25, 25, 25);
        }
    });

    ctx.globalAlpha = 1;
    var key;
    for (key in beeList) {
        var bee = beeList[key];
        bee.draw(Time.deltaTime);
    }

    for (key in queenList) {
        var queen = queenList[key];
        queen.draw(Time.deltaTime);
    }

    ctx.fillStyle = '#000000';
    ctx.fillText("Fps: " + Time.getFps(), 550, 10);

    setTimeout(() => draw(), 15);
};

class CircularQueue {
    public array = [];
    private start = 0;
    private end = 0;

    constructor(public length) {
        for (var i = 0; i < this.length; i++) {
            this.array[i] = null;
        }
    }

    push(value) {
        //If pushin another value will overfill, shift to the right.
        if ((this.end + 1) % this.length === this.start) {
            this.start = (this.start + 1) % this.length; //move the start forward one
            this.end = (this.end + 1) % this.length; //Move the end forward one
            this.array[this.end] = value; //Set the value at the end to override the old value.
        } else {
            this.end = (this.end + 1) % this.length; //Move the end forward one
            this.array[this.end] = value; //Set the value at the end to override the old value.
        }
    }

    at(index) {
        return this.array[(this.start + index) % this.length];
    }
}

class SimpleBee implements Interfaces.IPositionable {
    public size = 5;
    public inter: CircularQueue = new CircularQueue(5);

    private counter=0;

    constructor(public x, public y, public id, public color) {
        this.inter.push({ time: Time.currTime, x: x, y: y });
    }

    draw(deltaTime) {
        this.counter += deltaTime;
        ctx.fillStyle = this.color;
        var first = this.inter.at(0);
        var second = this.inter.at(1);

        if (first == null || second == null || !interpolate) {
            ctx.fillRect(this.x - this.size / 2, this.y - this.size / 2, this.size, this.size);
        } else {
            var x1 = first.x;
            var x2 = second.x;
            var y1 = first.y;
            var y2 = second.y;

            var timePercent = (this.counter*1000) / (second.time - first.time);

            var xLerp = lerp(x1, x2, timePercent);
            var yLerp = lerp(y1, y2, timePercent);

            ctx.fillRect(xLerp - this.size / 2, yLerp - this.size / 2, this.size, this.size);
        }
    }

    updatePosition(x, y) {
        this.inter.push({ time: Time.currTime, x: x, y: y });
        this.x = x;
        this.y = y;
        this.counter = 0;
    }
}
