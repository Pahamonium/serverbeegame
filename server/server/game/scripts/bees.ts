﻿import obj = require('./app/Objects');
import Bee = require('./app/Bee');
import consts = require('./app/consts');
import server = require('../../server');

var io = server.io;

export var serverControlled: boolean = false;

export var ctx;

export var beeList: { [index: number]: Bee.Bee } = {};
export var queenList: { [index: number]: Bee.Bee } = {};
export var grid: obj.Grid;

export var queenCounter = 1;

//FOR HTML STUFF
export var stats: HTMLElement;
export var sections: { [index: number]: obj.CustomElement } = {}; //A dictionary with a key (index) value.

var fpsCounter: number = 0;
var fpsTick: number = 1; //one second
var sendUpdateCounter: number = 0;
var sendUpdateTick: number = 0.5;


var sendGridDataCounter: number = 0;
var sendGridDataTick = 1;
var gridData = [];

//Where the game starts
export function start() {

    if (!serverControlled) {
        var canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("simulation_canvas");
        ctx = canvas.getContext("2d");
        canvas.width = consts.canvasWidth;
        canvas.height = consts.canvasHeight;
        stats = document.getElementById('stats');
    }

    //Initially calculate delta
    obj.Time.updateDelta();

    this.grid = new obj.Grid(consts.COLS, consts.ROWS);

    //Spawn the new queenList!
    for (var i = 0; i < consts.STARTING_QUEENS; i++) {
        var queen = new Bee.Bee(consts.QUEEN, '#' + (Math.random() * 0xFFFFFF << 0).toString(16), 0, Math.random() * grid.getLength(0) * consts.SQUARE_SIZE, Math.random() * grid.getLength(1) * consts.SQUARE_SIZE, consts.QUEEN_BASE_SIZE, consts.QUEEN_BASE_SPEED, 35, null, 100, consts.AGE_MAX_QUEEN);
        queen.queenRef = queen;
        this.addBee(queen);
    }

    update();
};

//The main game loop.
function update() {
    //Calculate delta
    obj.Time.updateDelta();

    fpsCounter += obj.Time.deltaTime;
    sendUpdateCounter += obj.Time.deltaTime;

    var sendUpdate: boolean = false;
    var updateList = [];

    //Calculate FPS every second
    if (fpsCounter >= fpsTick) {
        fpsCounter -= fpsTick;
        obj.Time.setFps();
    }

    if (sendUpdateCounter >= sendUpdateTick) {
        sendUpdateCounter -= sendUpdateTick;
        sendUpdate = true;
    }

    //Redraw the canvas
    if (!serverControlled) drawCanvas();

    updateList = updateBees(beeList, sendUpdate, updateList);
    updateList = updateBees(queenList, sendUpdate, updateList);

    sendGraphUpdate();

    if (sendUpdate) {
        io.sockets.emit('update', updateList);
    }

    if (!serverControlled) drawElements();

    setTimeout(() => update(), 15);
};

function updateBees(list, sendUpdate, updateList) {
    for (var key in list) {
        var currBee: Bee.Bee = list[key];
        if (currBee.health <= 0) {
            removeBee(currBee, list);
            continue;
        }
        currBee.update(obj.Time.deltaTime);

        //If not the server, do a draw call.
        if (!serverControlled) currBee.render(obj.Time.deltaTime);
        else {
            //Otherwise, if it is time to send an update, save the ID, X, and Y values to send.
            if (sendUpdate) {
                var data = { id: currBee.hash(), x: currBee.x, y: currBee.y, size: currBee.size };
                updateList.push(data);
            }
        }
    }

    return updateList;
}

//Draws html elements
function drawElements() {
    //Loop
    for (var key in sections) {
        var elem = sections[key];
        if (elem.queen.health <= 0) {
            stats.removeChild(elem.element);
            delete elem;
            delete sections[key];
            continue;
        }

        elem.element.innerHTML = "Queen " + elem.queen.num +
        "<br>Health: " + (~~(elem.queen.health * 10) / 10) +
        "<br>Food: " + ~~elem.queen.food +
        "<br>ColonySize: " + elem.queen.colonySize;
    }
}

//Clears the canvas and draws the food.
function drawCanvas() {
    ctx.globalAlpha = 1;
    ctx.fillStyle = "#D4BBA5";
    ctx.fillRect(0, 0, consts.canvasWidth, consts.canvasHeight);

    for (var i = 0; i < grid.getLength(0); i++)
        for (var j = 0; j < grid.getLength(1); j++) {
            var cell = this.grid.getCellByIndex(i, j);
            if (cell.food > 5) {
                ctx.globalAlpha = cell.food / 100;
                ctx.fillStyle = "#D60E00";
                ctx.fillRect(consts.SQUARE_SIZE * i, consts.SQUARE_SIZE * j, consts.SQUARE_SIZE, consts.SQUARE_SIZE);
            }

            if (cell.needsUpdating) {
                this.gridData.push({ col: cell.col, row: cell.row, food: cell.getFood() });
                cell.needsUpdating = false;
            }
        }

    ctx.globalAlpha = 1;
    ctx.fillStyle = "#000000";
    ctx.fillText("FPS: " + obj.Time.getFps(), consts.canvasWidth - 50, 20, 100);
}

function sendGraphUpdate() {

    //console.log('timer: ' + sendGridDataCounter+' delta: '+obj.Time.deltaTime);
    sendGridDataCounter += obj.Time.deltaTime;
    if (sendGridDataCounter >= sendGridDataTick) {
        sendGridDataCounter -= sendGridDataTick;

        gridData = [];

        grid.iterate((cell: obj.Cell) => {
            if (cell.needsUpdating) {
                var obj = { col: cell.col, row: cell.row, food: cell.getFood() };
                gridData.push(obj);
                cell.needsUpdating = false;
            }
        });

        io.emit('gridUpdate', gridData);
    } 
}

export function addSectionToHTML(queen: Bee.Bee) {
    if (!this.serverControlled) {

        var statsElement = document.getElementById('stats');
        var element = document.createElement('div');

        element.setAttribute('id', queen.hash().toString());
        element.setAttribute('class', 'stat');

        statsElement.appendChild(element);
        sections[queen.hash()] = new obj.CustomElement(element, queen);
    }
}

//Adds a bee to the list and sends it over the network.
export function addBee(bee: Bee.Bee) {
    //Get the right list.
    if (bee.caste === consts.QUEEN)
        beeList[bee.hash()] = bee;
    else
        queenList[bee.hash()] = bee;;

    //Send some info! (caste, id, x, y, color)
    var obj = { caste: bee.caste, id: bee.hash(), x: bee.x, y: bee.y, color: bee.color};
    io.emit('create', obj);
}

//Removes a bee from the list and sends the action over the network.
function removeBee(bee: Bee.Bee, list) {

    bee.die();
    //Send some info (caste, id)
    var obj = { caste: bee.caste, id: bee.hash() };
    io.emit('delete', obj);

    var remove = list[bee.hash()];
    delete list[bee.hash()];
    delete remove;
    
}

