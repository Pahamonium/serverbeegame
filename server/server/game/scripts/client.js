///<reference path="./app/consts.ts"/>
var _this = this;
var SimpleCell = (function () {
    function SimpleCell(col, row) {
        this.col = col;
        this.row = row;
        this.food = 0;
    }
    return SimpleCell;
})();
var SimpleGrid = (function () {
    function SimpleGrid(width, height, squareSize) {
        this.squareSize = squareSize;
        this.gridArray = new Array();
        this.cols = ~~(width / squareSize + 1);
        this.rows = ~~(height / squareSize + 1);
        for (var col = 0; col < this.cols; col++) {
            this.gridArray[col] = new Array();
            for (var row = 0; row < this.rows; row++) {
                this.gridArray[col][row] = new SimpleCell(col, row);
            }
        }
    }
    SimpleGrid.prototype.getCell = function (x, y) {
        var col = ~~(x / this.squareSize);
        var row = ~~(y / this.squareSize);
        return this.gridArray[col][row];
    };
    SimpleGrid.prototype.getCellByIndex = function (xIndex, yIndex) {
        return this.gridArray[xIndex][yIndex];
    };
    SimpleGrid.prototype.iterate = function (func) {
        for (var col = 0; col < this.gridArray.length; col++) {
            for (var row = 0; row < this.gridArray.length; row++) {
                func(this.gridArray[col][row]);
            }
        }
    };
    return SimpleGrid;
})();
console.log("Connected to server");
var socket = io.connect();
var ctx = (document.getElementById('simulation_canvas')).getContext("2d");
var beeList = {};
var queenList = {};
var interpolate = true;
var grid = new SimpleGrid(600, 600, 25);
socket.on('update', function (list) {
    var beeData, tmp;
    for (var i = 0; i < list.length; i++) {
        beeData = list[i]; //Cache the current object in the list
        if (beeData.caste === 3)
            tmp = queenList[beeData.id];
        else
            tmp = beeList[beeData.id];
        if (tmp) {
            //tmp.x = beeData.x;
            //tmp.y = beeData.y;
            tmp.updatePosition(beeData.x, beeData.y);
            tmp.size = beeData.size;
        }
    }
});
socket.on('enter', function (list) {
    for (var i = 0; i < list.length; i++) {
        var bee = list[i];
        if (bee.caste !== 3)
            _this.beeList[bee.id] = new SimpleBee(bee.x, bee.y, bee.id, bee.color);
        else {
            _this.queenList[bee.id] = new SimpleBee(bee.x, bee.y, bee.id, bee.color);
        }
    }
});
socket.on('grid', function (list) {
    var data, cell;
    for (var i = 0; i < list.length; i++) {
        data = list[i];
        cell = _this.grid.getCellByIndex(data.col, data.row);
        cell.food = data.food;
    }
});
socket.on('gridUpdate', function (list) {
    var data;
    for (var i = 0; i < list.length; i++) {
        data = list[i];
        _this.grid.getCellByIndex(data.col, data.row).food = data.food;
    }
});
socket.on('create', function (bee) {
    if (bee.caste === 3) {
        queenList[bee.id] = new SimpleBee(bee.x, bee.y, bee.id, bee.color);
    }
    else {
        beeList[bee.id] = new SimpleBee(bee.x, bee.y, bee.id, bee.color);
    }
});
socket.on('delete', function (bee) {
    var remove;
    console.log('removedBee');
    if (bee.caste === 3) {
        remove = queenList[bee.id];
        delete queenList[bee.id];
        delete remove;
    }
    else {
        remove = beeList[bee.id];
        delete beeList[bee.id];
        delete remove;
    }
});
var Time = (function () {
    function Time() {
    }
    Time.calcDelta = function () {
        this.lastTime = this.currTime;
        this.currTime = new Date().getTime();
        this.deltaTime = (this.currTime - this.lastTime) / 1000;
    };
    Time.getFps = function () {
        return this.fps;
    };
    Time.calcFps = function () {
        this.fps = ~~(1 / this.deltaTime);
    };
    Time.lastTime = 0;
    Time.currTime = 0;
    Time.deltaTime = 0;
    Time.fps = 0;
    return Time;
})();
var fpsCounter = 0, fpsTick = 1;
function lerp(a, b, t) {
    return a + t * (b - a);
}
(function start() {
    Time.calcDelta();
    draw();
})();
function draw() {
    ctx.fillStyle = '#A8A58D';
    ctx.fillRect(0, 0, 600, 600);
    Time.calcDelta();
    fpsCounter += Time.deltaTime;
    if (fpsCounter >= fpsTick) {
        fpsCounter -= fpsTick;
        Time.calcFps();
    }
    ctx.fillStyle = '#E34C2D';
    this.grid.iterate(function (cell) {
        if (cell.food > 0) {
            ctx.globalAlpha = cell.food / 100;
            ctx.fillRect(cell.col * 25, cell.row * 25, 25, 25);
        }
    });
    ctx.globalAlpha = 1;
    var key;
    for (key in beeList) {
        var bee = beeList[key];
        bee.draw(Time.deltaTime);
    }
    for (key in queenList) {
        var queen = queenList[key];
        queen.draw(Time.deltaTime);
    }
    ctx.fillStyle = '#000000';
    ctx.fillText("Fps: " + Time.getFps(), 550, 10);
    setTimeout(function () { return draw(); }, 15);
}
;
var CircularQueue = (function () {
    function CircularQueue(length) {
        this.length = length;
        this.array = [];
        this.start = 0;
        this.end = 0;
        for (var i = 0; i < this.length; i++) {
            this.array[i] = null;
        }
    }
    CircularQueue.prototype.push = function (value) {
        //If pushin another value will overfill, shift to the right.
        if ((this.end + 1) % this.length === this.start) {
            this.start = (this.start + 1) % this.length; //move the start forward one
            this.end = (this.end + 1) % this.length; //Move the end forward one
            this.array[this.end] = value; //Set the value at the end to override the old value.
        }
        else {
            this.end = (this.end + 1) % this.length; //Move the end forward one
            this.array[this.end] = value; //Set the value at the end to override the old value.
        }
    };
    CircularQueue.prototype.at = function (index) {
        return this.array[(this.start + index) % this.length];
    };
    return CircularQueue;
})();
var SimpleBee = (function () {
    function SimpleBee(x, y, id, color) {
        this.x = x;
        this.y = y;
        this.id = id;
        this.color = color;
        this.size = 5;
        this.inter = new CircularQueue(5);
        this.counter = 0;
        this.inter.push({ time: Time.currTime, x: x, y: y });
    }
    SimpleBee.prototype.draw = function (deltaTime) {
        this.counter += deltaTime;
        ctx.fillStyle = this.color;
        var first = this.inter.at(0);
        var second = this.inter.at(1);
        if (first == null || second == null || !interpolate) {
            ctx.fillRect(this.x - this.size / 2, this.y - this.size / 2, this.size, this.size);
        }
        else {
            var x1 = first.x;
            var x2 = second.x;
            var y1 = first.y;
            var y2 = second.y;
            var timePercent = (this.counter * 1000) / (second.time - first.time);
            var xLerp = lerp(x1, x2, timePercent);
            var yLerp = lerp(y1, y2, timePercent);
            ctx.fillRect(xLerp - this.size / 2, yLerp - this.size / 2, this.size, this.size);
        }
    };
    SimpleBee.prototype.updatePosition = function (x, y) {
        this.inter.push({ time: Time.currTime, x: x, y: y });
        this.x = x;
        this.y = y;
        this.counter = 0;
    };
    return SimpleBee;
})();
//# sourceMappingURL=client.js.map