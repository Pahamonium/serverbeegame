/// <reference path="consts.ts"/>
/// <reference path="Interfaces.ts"/>
/// <reference path="../bees.ts"/>
var obj = require('./Objects');
var bees = require('../bees');
var consts = require('./consts');
var Bee = (function () {
    function Bee(caste, color, age, x, y, size, speed, food, queenRef, health, maxAge) {
        this.caste = caste;
        this.color = color;
        this.age = age;
        this.x = x;
        this.y = y;
        this.size = size;
        this.speed = speed;
        this.food = food;
        this.queenRef = queenRef;
        this.health = health;
        this.maxAge = maxAge;
        this.destX = 0;
        this.destY = 0;
        this.moveDis = 50;
        this.foodCapacity = 20;
        this.colonySize = 0;
        this.num = 0;
        this.damage = 10;
        this.objectivesTimer = 0;
        this.objectivesTick = 0.5; //Second
        this.attackTimer = 0;
        this.attackTick = 0.3;
        this.currCell = null;
        this.id = 0;
        this.id = Math.random() * Number.MAX_VALUE;
        if (this.queenRef)
            this.queenRef.addAnt();
        if (this.caste === consts.QUEEN) {
            bees.addSectionToHTML(this);
            this.num = bees.queenCounter++;
        }
    }
    Bee.prototype.die = function () {
        if (this.queenRef)
            this.queenRef.removeAnt();
        this.currCell.removeAnt(this);
        this.currCell.addFood(this.food);
    };
    //The update call for this Bee object
    Bee.prototype.update = function (deltaTime) {
        //This will run the function 'setDestination' on a timer.
        if (this.objectivesTimer >= this.objectivesTick) {
            this.objectivesTimer -= this.objectivesTick;
            this.setDestination(this);
        }
        this.objectivesTimer += deltaTime; //Update the timer.
        this.updateCell(this);
        //Moves the Bee
        this.move(this, deltaTime);
        if (this.caste !== consts.QUEEN)
            this.checkForEnemies(deltaTime);
        this.antAging(this);
        this.eat(this, deltaTime);
        this.checkFood(this, deltaTime);
        this.spawnLarva();
        this.checkDeath(this);
    };
    //The render call for this Bee object
    Bee.prototype.render = function (deltaTime) {
        bees.ctx.fillStyle = this.color; //Set the color
        bees.ctx.fillRect(this.x - ~~(this.size / 2), this.y - ~~(this.size / 2), this.size, this.size); //Draw the Bee!
        if (this.caste === consts.QUEEN) {
            bees.ctx.fillStyle = '#000000';
            bees.ctx.strokeRect(this.x - ~~(this.size / 2), this.y - ~~(this.size / 2), this.size, this.size);
            bees.ctx.fillStyle = '#401B16';
            bees.ctx.fillText(this.num.toString(), this.x - 2, this.y + 3);
        }
    };
    Bee.prototype.checkForEnemies = function (deltaTime) {
        this.attackTimer += deltaTime;
        if (this.attackTimer >= this.attackTick) {
            this.attackTimer -= this.attackTick;
            var startCol = ~~(this.currCell.col - 1);
            var startRow = ~~(this.currCell.row - 1);
            var endCol = ~~(this.currCell.col + 1);
            var endRow = ~~(this.currCell.row + 1);
            for (var col = startCol; col <= endCol; col++) {
                for (var row = startRow; row <= endRow; row++) {
                    if (!bees.grid.isIndexInsideGrid(col, row))
                        continue;
                    var tmpAnts = bees.grid.getCellByIndex(col, row).getAnts();
                    for (var key in tmpAnts) {
                        var otherAnt = tmpAnts[key];
                        if (otherAnt.queenRef === this.queenRef)
                            continue;
                        var combinedSize = this.size / 2 + otherAnt.size / 2;
                        var dst = (Math.abs(otherAnt.x - this.x) <= combinedSize && Math.abs(otherAnt.y - this.y) <= combinedSize);
                        //console.log("Checking");
                        if (dst) {
                            if (otherAnt.cast === consts.QUEEN) {
                                this.food += this.damage * 0.5;
                                otherAnt.health -= this.damage;
                            }
                            else {
                                if (this.size > otherAnt.size) {
                                    this.food += this.damage * 0.25;
                                    this.health -= this.damage * 0.25;
                                    otherAnt.health -= this.damage;
                                }
                                else {
                                    otherAnt.food += this.damage * 0.25;
                                    otherAnt.health -= this.damage * 0.25;
                                    this.health -= this.damage;
                                }
                            }
                            return;
                        }
                    }
                }
            }
        }
    };
    Bee.prototype.setDestination = function (bee) {
        //Cache the current Cell and reduce some pheromone on this Cell.
        var currCell = bees.grid.getGrid()[~~(bee.x / consts.SQUARE_SIZE)][~~(bee.y / consts.SQUARE_SIZE)];
        //Set destination.
        if (bees.grid.isPointInsideGrid(bee.x, bee.y)) {
            //If the bee is a worker and has 0 food, look around for a pheromone trail
            if (bee.caste === consts.WORKER && bee.food <= 0) {
                //Start and End indexes
                var startCol = ~~((bee.x / consts.SQUARE_SIZE - 1));
                var startRow = ~~((bee.y / consts.SQUARE_SIZE) - 1);
                var endCol = ~~((bee.x / consts.SQUARE_SIZE) + 1);
                var endRow = ~~((bee.y / consts.SQUARE_SIZE) + 1);
                for (var col = startCol; col <= endCol; col++) {
                    for (var row = startRow; row <= endRow; row++) {
                        if (col != startCol && col != endCol && row != startRow && row != endRow)
                            continue;
                        if (!bees.grid.isIndexInsideGrid(col, row))
                            continue;
                        if (bees.grid.getGrid()[col][row].pheromone > 0.5) {
                            bee.destX = col * consts.SQUARE_SIZE + consts.SQUARE_SIZE / 2;
                            bee.destY = row * consts.SQUARE_SIZE + consts.SQUARE_SIZE / 2;
                            return;
                        }
                    }
                }
            }
            if (bee.food > 0 && bee.caste === consts.WORKER) {
                bee.destX = bee.queenRef.x;
                bee.destY = bee.queenRef.y;
            }
            else {
                //If the bee is a queen or we have a random number under 97%, move randomly
                if (bee.caste === consts.QUEEN || Math.random() < 0.97) {
                    bee.destX = bee.x + bee.moveDis * Math.random() - bee.moveDis * 0.5;
                    bee.destY = bee.y + bee.moveDis * Math.random() - bee.moveDis * 0.5;
                }
                else {
                    bee.destX = bee.queenRef.x;
                    bee.destY = bee.queenRef.y;
                }
            }
        }
    };
    Bee.prototype.spawnLarva = function () {
        if (this.caste === consts.QUEEN) {
            //If enough food, have a chance to spawn an egg.
            if (Math.random() < 0.25 && this.food >= 4) {
                var newBee = new Bee(consts.LARVA, this.color, 0, this.x, this.y, consts.LARVA_BASE_SIZE, consts.LARVA_BASE_SPEED, 0, this, 100, consts.AGE_MAX_LARVA);
                newBee.destX = this.x; //set destX and destY
                newBee.destY = this.y;
                this.food -= 4; //Subtract food.
                this.size += 0.01; //Grow!
                this.speed += 0.00001; //Increase speed!
                bees.addBee(newBee);
            }
        }
    };
    //Checks if the ant's age is too great or if the health is 0 or below.
    Bee.prototype.checkDeath = function (ant) {
        if (ant.age > ant.maxAge || ant.health <= 0)
            ant.health = 0;
    };
    //Allows the ant to eat for growing!
    Bee.prototype.eat = function (bee, deltaTime) {
        if (bee.caste === consts.QUEEN && bee.food > 0) {
            bee.food -= 1 * deltaTime;
            bee.size += 0.05 * deltaTime;
        }
        else if (bee.food > 0) {
            bee.food -= 0.5 * deltaTime;
            bee.size += 0.05 * deltaTime;
        }
    };
    //Ages the beeList
    Bee.prototype.antAging = function (bee) {
        bee.age += obj.Time.deltaTime;
        if (bee.caste === consts.LARVA) {
            //If the bee is ready to age to a worker, do it!
            if (bee.age > consts.AGE_MAX_LARVA) {
                this.morph(bee);
            }
            else {
                bee.destX = bee.queenRef.x;
                bee.destY = bee.queenRef.y;
            }
        }
    };
    //Morphs the beeList
    Bee.prototype.morph = function (bee) {
        if (bee.caste === consts.LARVA) {
            //Morph into worker
            if (Math.random() < .995) {
                bee.caste = consts.WORKER;
                bee.age = 0;
                bee.health = 100;
                bee.maxAge = consts.AGE_MAX_WORKER;
                bee.speed = consts.WORKER_BASE_SPEED;
            }
        }
    };
    //Checks if the ant has enough food.
    Bee.prototype.checkFood = function (ant, deltaTime) {
        if (ant.food <= 0)
            ant.health -= deltaTime * 1;
        else {
            if (ant.health < 100) {
                ant.health = ((ant.health + deltaTime * 0.5) >= 100) ? 100 : ant.health + deltaTime * 0.5;
            }
        }
    };
    //Updates the Cell based on the position of this Bee
    Bee.prototype.updateCell = function (ant) {
        var cell = bees.grid.getCell(this); //Get the cell at the current location
        //If the currentCell of this Bee is null or the two cells don't match, fix it!
        if (!this.currCell || this.currCell != cell) {
            if (this.currCell)
                this.currCell.removeAnt(this);
            cell.addAnt(this);
            this.currCell = cell;
        }
    };
    Bee.prototype.addAnt = function (ant) {
        this.colonySize++;
    };
    Bee.prototype.removeAnt = function (ant) {
        this.colonySize--;
    };
    Bee.prototype.move = function (bee, deltaTime) {
        if (bee.food <= 0)
            this.currCell.pheromone *= 0.25 * obj.Time.deltaTime;
        while (this.currCell.getFood() > 2 && bee.food < 8) {
            this.currCell.removeFood(2);
            bee.food += 2;
        }
        if (this.caste !== consts.QUEEN) {
            //Drop pheromone if they have food.
            this.currCell.pheromone += bee.food * 10;
            if (Math.abs(bee.queenRef.y - bee.y) + Math.abs(bee.queenRef.x - bee.x) < 0.4) {
                bee.queenRef.food += bee.food;
                bee.food = 0;
            }
        }
        var rotation = Math.atan2((bee.destY - bee.y), (bee.destX - bee.x));
        if (Math.abs(bee.y - bee.destY) + Math.abs(bee.x - bee.destX) > 0.2) {
            bee.x += bee.speed * Math.cos(rotation) * deltaTime;
            bee.y += bee.speed * Math.sin(rotation) * deltaTime;
        }
        //Boundary cases.
        if (bee.x < 0)
            bee.x = 0;
        else if (bee.x / consts.SQUARE_SIZE >= bees.grid.getLength(0))
            bee.x = bees.grid.getLength(0) * consts.SQUARE_SIZE - 1;
        if (bee.y < 0)
            bee.y = 0;
        else if (bee.y / consts.SQUARE_SIZE >= bees.grid.getLength(1))
            bee.y = bees.grid.getLength(1) * consts.SQUARE_SIZE - 1;
    };
    Bee.prototype.hash = function () {
        return this.id;
    };
    return Bee;
})();
exports.Bee = Bee;
//# sourceMappingURL=Bee.js.map