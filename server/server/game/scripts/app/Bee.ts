﻿/// <reference path="consts.ts"/>
/// <reference path="Interfaces.ts"/>
/// <reference path="../bees.ts"/>

import obj = require('./Objects');
import bees = require('../bees');
import consts = require('./consts');

export class Bee implements Interfaces.IUpdateable, Interfaces.IDrawable, Interfaces.IHashable, Interfaces.IPositionable {
    public destX: number = 0;
    public destY: number = 0;
    public moveDis: number = 50;
    public halfMoveDis: number = this.moveDis * 0.5;
    public foodCapacity: number = 20;
    public colonySize: number = 0;
    public num: number = 0;
    public damage:number = 10;

    private objectivesTimer: number = 0;
    private objectivesTick: number = 0.5; //Second
    private attackTimer: number = 0;
    private attackTick: number = 0.3;
    private currCell: obj.Cell = null;
    private id: number = 0;

    constructor(public caste: number, public color, public age, public x, public y, public size, public speed, public food, public queenRef, public health, public maxAge) {
        this.id = Math.random() * Number.MAX_VALUE;
        if (this.queenRef) this.queenRef.addAnt();
        if (this.caste === consts.QUEEN) {
            bees.addSectionToHTML(this);
            this.num = bees.queenCounter++;
        }
    }

    die() {
        if (this.queenRef) this.queenRef.removeAnt();
        this.currCell.removeAnt(this);
        this.currCell.addFood(this.food);
    }

    //The update call for this Bee object
    update(deltaTime: number) {
        //This will run the function 'antsMorphsAndObjectives' on a timer.
        if (this.objectivesTimer >= this.objectivesTick) {
            this.objectivesTimer -= this.objectivesTick;
            this.antsMorphsAndObjectives(this);
        }
        this.objectivesTimer += deltaTime; //Update the timer.

        this.updateCell(this);
        //Moves the Bee
        this.move(this, deltaTime);

        if (this.caste !== consts.QUEEN)
            this.checkForEnemies(deltaTime);

        this.antAging(this);
        this.eat(this, deltaTime);
        this.checkFood(this, deltaTime);
        this.checkDeath(this);
           
    }

    //The render call for this Bee object
    render(deltaTime: number) {
        bees.ctx.fillStyle = this.color; //Set the color
        bees.ctx.fillRect(this.x - ~~(this.size / 2), this.y - ~~(this.size / 2), this.size, this.size); //Draw the Bee!

        if (this.caste === consts.QUEEN) {
            bees.ctx.fillStyle = '#000000';
            bees.ctx.strokeRect(this.x - ~~(this.size / 2), this.y - ~~(this.size / 2), this.size, this.size);
            bees.ctx.fillStyle = '#401B16';
            bees.ctx.fillText(this.num.toString(), this.x - 2, this.y + 3);
        }

    }

    checkForEnemies(deltaTime: number) {
        this.attackTimer += deltaTime;

        if (this.attackTimer >= this.attackTick) {
            this.attackTimer -= this.attackTick;

            var startCol = ~~(this.currCell.col - 1);
            var startRow = ~~(this.currCell.row - 1);
            var endCol = ~~(this.currCell.col + 1);
            var endRow = ~~(this.currCell.row + 1);


            for (var col = startCol; col <= endCol; col++) {
                for (var row = startRow; row <= endRow; row++) {
                    if (!bees.grid.isIndexInsideGrid(col, row)) continue;

                    var tmpAnts = bees.grid.getCellByIndex(col, row).getAnts();

                    for (var key in tmpAnts) {
                        var otherAnt = tmpAnts[key];
                        if (otherAnt.queenRef === this.queenRef) continue;

                        var combinedSize = this.size / 2 + otherAnt.size / 2;
                        var dst = (Math.abs(otherAnt.x - this.x) <= combinedSize && Math.abs(otherAnt.y - this.y) <= combinedSize);
                        //console.log("Checking");
                        if (dst) {
                            if (otherAnt.cast === consts.QUEEN) {
                                this.food += this.damage*0.5;
                                otherAnt.health -= this.damage;
                            } else {
                                if (this.size > otherAnt.size) {
                                    this.food += this.damage * 0.25;
                                    this.health -= this.damage * 0.25;
                                    otherAnt.health -= this.damage;
                                } else {
                                    otherAnt.food += this.damage * 0.25;
                                    otherAnt.health -= this.damage * 0.25;
                                    this.health -= this.damage;
                                }
                            }

                            return;
                        }
                    }
                }
            }
        }
    }

antsMorphsAndObjectives(bee: Bee) {
    //Cache the current Cell and reduce some pheromone on this Cell.
    var currCell: obj.Cell = bees.grid.getGrid()[~~(bee.x / consts.SQUARE_SIZE)][~~(bee.y / consts.SQUARE_SIZE)];

    //Set destination.
    if (bees.grid.isPointInsideGrid(bee.x, bee.y)) {

        if (bee.caste === consts.WORKER && bee.food <= 0) {

            var startCol = ~~((bee.x / consts.SQUARE_SIZE - 1));
            var startRow = ~~((bee.y / consts.SQUARE_SIZE) - 1);
            var endCol = ~~((bee.x / consts.SQUARE_SIZE) + 1);
            var endRow = ~~((bee.y / consts. SQUARE_SIZE) + 1);

            for (var col = startCol; col <= endCol; col++) {
                for (var row = startRow; row <= endRow; row++) {
                    if (col != startCol && col != endCol && row != startRow && startRow != endRow)
                        continue;

                    if (!bees.grid.isIndexInsideGrid(col, row))
                        continue;

                    if (bees.grid.getGrid()[col][row].pheromone > 0.5) {

                        bee.destX = col * consts.SQUARE_SIZE + consts.SQUARE_SIZE / 2;
                        bee.destY = row * consts.SQUARE_SIZE + consts.SQUARE_SIZE / 2;
                        return;
                    }
                }
            }

        } else {
        if (bee.caste === consts.QUEEN) {
                bee.destX = bee.x + bee.moveDis * Math.random() - bee.halfMoveDis;
                bee.destY = bee.y + bee.moveDis * Math.random() - bee.halfMoveDis;
            } else {
                if (Math.random() < 0.97) {
                    bee.destX = bee.x + bee.moveDis * Math.random() - bee.halfMoveDis;
                    bee.destY = bee.y + bee.moveDis * Math.random() - bee.halfMoveDis;
                } else {
                    bee.destX = bee.queenRef.x;
                    bee.destY = bee.queenRef.y;
                }
            }
        }
    }

    //Queen.
    if (bee.caste === consts.QUEEN) {

        //If enough food, have a chance to spawn an egg.
        if (Math.random() < 0.25 && bee.food >= 4) {
            var newBee = new Bee(consts.LARVA, bee.color, 0, bee.x, bee.y, consts.LARVA_BASE_SIZE, consts.LARVA_BASE_SPEED, 0, bee, 100, consts.AGE_MAX_LARVA);

            newBee.destX = bee.x; //set destX and destY
            newBee.destY = bee.y;
            bee.food -= 4; //Subtract food.
            bee.size += 0.01; //Grow!
            bee.speed += 0.00001; //Increase speed!

            bees.addBee(newBee);
        }
        //Chance to move randomly
        if (Math.random() < 0.6) {
            bee.destX = bee.x + 20 * Math.random() - 10;
            bee.destY = bee.y + 20 * Math.random() - 10;
        }
    }

    //Worker.
    else if (bee.caste === consts.WORKER) {


        if (bee.food > 0) {
            bee.destX = bee.queenRef.x;
            bee.destY = bee.queenRef.y;
            //Otherwise, move randomly (small chance to move towards the queen).
        } else {
            if (Math.random() < 0.97) {
                bee.destX = bee.x + bee.moveDis * Math.random() - bee.halfMoveDis;
                bee.destY = bee.y + bee.moveDis * Math.random() - bee.halfMoveDis;
            } else {
                bee.destX = bee.queenRef.x;
                bee.destY = bee.queenRef.y;
            }
        }

    }
}

    //Checks if the ant's age is too great or if the health is 0 or below.
    checkDeath(ant: Bee) {
        if (ant.age > ant.maxAge || ant.health <= 0)
            ant.health = 0;
    }

    //Allows the ant to eat for growing!
    eat(bee: Bee, deltaTime: number) {
        if (bee.caste === consts.QUEEN && bee.food > 0) {
            bee.food -= 1 * deltaTime;
            bee.size += 0.05 * deltaTime
        } else if (bee.food > 0) {
            bee.food -= 0.5 * deltaTime;
            bee.size += 0.05 * deltaTime;
        }
    }

    //Ages the beeList
    antAging(bee: Bee) {
        bee.age += obj.Time.deltaTime;

        if (bee.caste === consts.LARVA) {
            //If the bee is ready to age to a worker, do it!
            if (bee.age > consts.AGE_MAX_LARVA) {
                this.morph(bee);
            } else {
                bee.destX = bee.queenRef.x;
                bee.destY = bee.queenRef.y;
            }
            //Egg
        }
    }

    //Morphs the beeList
    morph(bee: Bee) {
        if (bee.caste === consts.LARVA) {
            //Morph into worker
            if (Math.random() < .995) {
                bee.caste = consts.WORKER;
                bee.age = 0;
                bee.health = 100;
                bee.maxAge = consts.AGE_MAX_WORKER;
                bee.speed = consts.WORKER_BASE_SPEED;
                //2% chance to morph into a new queen.
            }
        }
    }

    //Checks if the ant has enough food.
    checkFood(ant: Bee, deltaTime: number) {
        if (ant.food <= 0)
            ant.health -= deltaTime * 1;
        else {
            if (ant.health < 100) {
                ant.health = ((ant.health + deltaTime * 0.5) >= 100) ? 100 : ant.health + deltaTime * 0.5;
            }
        }
    }

    //Updates the Cell based on the position of this Bee
    updateCell(ant: Bee) {
        var cell: obj.Cell = bees.grid.getCell(this); //Get the cell at the current location
        //If the currentCell of this Bee is null or the two cells don't match, fix it!
        if (!this.currCell || this.currCell != cell) {
            if (this.currCell) this.currCell.removeAnt(this);
            cell.addAnt(this);
            this.currCell = cell;
        }
    }

    addAnt(ant: Bee) {
        this.colonySize++;
    }

    removeAnt(ant: Bee) {
        this.colonySize--;
    }

    move(bee: Bee, deltaTime: number): void {
        this.currCell.pheromone *= 0.25 * obj.Time.deltaTime;


        //Gather food from the current cell.
        while (this.currCell.getFood() > 2 && bee.food < 8) {
            this.currCell.removeFood(2);
            bee.food += 2;
        }

        if (this.caste !== consts.QUEEN) {
            //Drop pheromone if they have food.
            this.currCell.pheromone += bee.food*10;
            if (Math.abs(bee.queenRef.y - bee.y) + Math.abs(bee.queenRef.x - bee.x) < 0.4) {
                bee.queenRef.food += bee.food;
                bee.food = 0;
            }
        }

        var rotation = Math.atan2((bee.destY - bee.y), (bee.destX - bee.x));
        if (Math.abs(bee.y - bee.destY) + Math.abs(bee.x - bee.destX) > 0.2) {
            bee.x += bee.speed * Math.cos(rotation) * deltaTime;
            bee.y += bee.speed * Math.sin(rotation) * deltaTime;
        }
        //Boundary cases.
        if (bee.x < 0)
            bee.x = 0;
        else if (bee.x / consts.SQUARE_SIZE >= bees.grid.getLength(0))
            bee.x = bees.grid.getLength(0) * consts.SQUARE_SIZE - 1;
        if (bee.y < 0)
            bee.y = 0;
        else if (bee.y / consts.SQUARE_SIZE >= bees.grid.getLength(1))
            bee.y = bees.grid.getLength(1) * consts.SQUARE_SIZE - 1;
    }

    hash() {
        return this.id;
    }
}
