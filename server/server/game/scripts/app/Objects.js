/// <reference path="consts.ts"/>
/// <reference path="Interfaces.ts"/>
var consts = require('./consts');
var Time = (function () {
    function Time() {
    }
    Time.updateDelta = function () {
        this.lastTime = this.currTime;
        this.currTime = new Date().getTime();
        this.deltaTime = (this.currTime - this.lastTime) / 1000;
    };
    Time.getFps = function () {
        return this.fps;
    };
    Time.setFps = function () {
        this.fps = ~~(1 / this.deltaTime);
    };
    Time.lastTime = 0;
    Time.currTime = 0;
    Time.deltaTime = 0;
    Time.fps = 0;
    return Time;
})();
exports.Time = Time;
//This is the grid object to hold our grid (Cells)
var Grid = (function () {
    //Constructs the grid. Fills out the 2D array with new Cells.
    function Grid(cols, rows) {
        this.cols = cols;
        this.rows = rows;
        this.gridArray = new Array(cols);
        for (var col = 0; col < this.gridArray.length; col++) {
            this.gridArray[col] = new Array(rows);
            for (var row = 0; row < this.gridArray[col].length; row++) {
                this.gridArray[col][row] = new Cell(col, row);
                if (Math.random() <= 0.15) {
                    this.gridArray[col][row].food = 25 + Math.random() * 50;
                }
            }
        }
    }
    //Gets the length of the given dimension.
    Grid.prototype.getLength = function (dimension) {
        return (dimension == 0) ? this.gridArray.length : this.gridArray[0].length;
    };
    //Gets the grid for accessing.
    Grid.prototype.getGrid = function () {
        return this.gridArray;
    };
    //Returns true if the point is inside the graph, false otherwise.
    Grid.prototype.isPointInsideGrid = function (x, y) {
        var xIndex = ~~(x / consts.SQUARE_SIZE);
        var yIndex = ~~(y / consts.SQUARE_SIZE);
        return this.isIndexInsideGrid(xIndex, yIndex);
    };
    //Returns true if the index is inside the graph, false otherwise.
    Grid.prototype.isIndexInsideGrid = function (xIndex, yIndex) {
        return xIndex >= 0 && xIndex < this.gridArray.length && yIndex >= 0 && yIndex < this.gridArray[0].length;
    };
    //Gets the Cell of a given position. This uses the IPositionable interface so that any entity with a 
    //position can use this.
    Grid.prototype.getCell = function (position) {
        return this.gridArray[~~(position.x / consts.SQUARE_SIZE)][~~(position.y / consts.SQUARE_SIZE)];
    };
    Grid.prototype.getCellByIndex = function (xIndex, yIndex) {
        return this.gridArray[xIndex][yIndex];
    };
    Grid.prototype.iterate = function (func) {
        for (var col = 0; col < this.gridArray.length; col++) {
            for (var row = 0; row < this.gridArray[0].length; row++) {
                func(this.gridArray[col][row]);
            }
        }
    };
    return Grid;
})();
exports.Grid = Grid;
//The Cell class to be used in a grid.
var Cell = (function () {
    function Cell(col, row) {
        this.col = col;
        this.row = row;
        this.food = 0;
        this.pheromone = 0;
        this.antsDict = {}; //A dictionary with a key (index) value.
    }
    //Adds an Bee to this Cell's dictionary.
    Cell.prototype.addAnt = function (ant) {
        this.antsDict[ant.hash()] = ant;
    };
    //Removes an Bee from this Cell's dictionary.
    Cell.prototype.removeAnt = function (ant) {
        delete this.antsDict[ant.hash()];
    };
    Cell.prototype.getAnts = function () {
        return this.antsDict;
    };
    Cell.prototype.addFood = function (amount) {
        this.food += amount;
        this.needsUpdating = true;
    };
    Cell.prototype.removeFood = function (amount) {
        this.food -= amount;
        this.needsUpdating = true;
    };
    Cell.prototype.getFood = function () {
        return this.food;
    };
    return Cell;
})();
exports.Cell = Cell;
var CustomElement = (function () {
    function CustomElement(element, queen) {
        this.element = element;
        this.queen = queen;
    }
    return CustomElement;
})();
exports.CustomElement = CustomElement;
//# sourceMappingURL=Objects.js.map