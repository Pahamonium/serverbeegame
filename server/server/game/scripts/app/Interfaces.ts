﻿
module Interfaces {
    export interface IPositionable {
        x: number;
        y: number;
    }

    export interface IHashable {
        hash();
    }

    export interface IUpdateable {
        update(deltaTime);
    }

    export interface IDrawable {
        render(deltaTime);
    }
}