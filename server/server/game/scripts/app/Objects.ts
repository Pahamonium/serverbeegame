﻿/// <reference path="consts.ts"/>
/// <reference path="Interfaces.ts"/>

import Bee = require('./Bee');
import consts = require('./consts');

export class Time {
    public static lastTime = 0;
    public static currTime = 0;
    public static deltaTime = 0;
    private static fps = 0;

    static updateDelta() {
        this.lastTime = this.currTime;
        this.currTime = new Date().getTime();
        this.deltaTime = (this.currTime - this.lastTime)/1000;
    }

    static getFps(): number {
        return this.fps;
    }

    static setFps() {
        this.fps = ~~(1 / this.deltaTime);
    }
}


//This is the grid object to hold our grid (Cells)
export class Grid {
    private gridArray;

    //Constructs the grid. Fills out the 2D array with new Cells.
    constructor(public cols: number, public rows: number) {
        this.gridArray = new Array(cols);
        for (var col = 0; col < this.gridArray.length; col++) {
            this.gridArray[col] = new Array(rows);
            for (var row = 0; row < this.gridArray[col].length; row++) {
                this.gridArray[col][row] = new Cell(col, row);
                if (Math.random() <= 0.15) {
                    this.gridArray[col][row].food = 25 + Math.random() * 50;
                }
            }
        }
    }

    //Gets the length of the given dimension.
    getLength(dimension: number) {
        return (dimension == 0) ? this.gridArray.length : this.gridArray[0].length;
    }

    //Gets the grid for accessing.
    getGrid() {
        return this.gridArray;
    }

    //Returns true if the point is inside the graph, false otherwise.
    isPointInsideGrid(x: number, y: number): boolean {
        var xIndex = ~~(x / consts.SQUARE_SIZE);
        var yIndex = ~~(y / consts.SQUARE_SIZE);

        return this.isIndexInsideGrid(xIndex, yIndex);
    }

    //Returns true if the index is inside the graph, false otherwise.
    isIndexInsideGrid(xIndex: number, yIndex: number): boolean {
        return xIndex >= 0 && xIndex < this.gridArray.length && yIndex >= 0 && yIndex < this.gridArray[0].length;
    }

    //Gets the Cell of a given position. This uses the IPositionable interface so that any entity with a 
    //position can use this.
    getCell(position: Interfaces.IPositionable) {
        return this.gridArray[~~(position.x / consts.SQUARE_SIZE)][~~(position.y / consts.SQUARE_SIZE)];
    }

    getCellByIndex(xIndex: number, yIndex: number) {
        return this.gridArray[xIndex][yIndex];
    }

    iterate(func) {
        for (var col = 0; col < this.gridArray.length; col++) {
            for (var row = 0; row < this.gridArray[0].length; row++) {
                func(this.gridArray[col][row]);
            }
        }
    }

}

//The Cell class to be used in a grid.
export class Cell {
    private food = 0;
    public pheromone = 0;
    public antsDict: { [index: number]: Bee.Bee } = {}; //A dictionary with a key (index) value.
    public needsUpdating : boolean;

    constructor(public col, public row) {

    }

    //Adds an Bee to this Cell's dictionary.
    addAnt(ant: Bee.Bee) {
        this.antsDict[ant.hash()] = ant;
    }

    //Removes an Bee from this Cell's dictionary.
    removeAnt(ant: Bee.Bee) {
        delete this.antsDict[ant.hash()];
    }

    getAnts() {
        return this.antsDict;
    }

    addFood(amount:number) {
        this.food += amount;
        this.needsUpdating = true;
    }

    removeFood(amount: number) {
        this.food -= amount;
        this.needsUpdating = true;
    }

    getFood() {
        return this.food;
    }
}

export class CustomElement {
    constructor(public element: HTMLElement, public queen: Bee.Bee) {

    }
}
