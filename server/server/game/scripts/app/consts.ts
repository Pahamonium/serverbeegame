export var canvasWidth = 600;
export var canvasHeight = 600;
export var SQUARE_SIZE = 25;
export var COLS = canvasWidth / SQUARE_SIZE;
export var ROWS = canvasHeight / SQUARE_SIZE;

export var LARVA = 0;
export var WORKER = 1;
export var QUEEN = 2;

export var STARTING_QUEENS = 10;

export var QUEEN_BASE_SIZE = 15;
export var WORKER_BASE_SIZE = 10;
export var LARVA_BASE_SIZE = 3;
export var EGG_BASE_SIZE = 1;

export var QUEEN_BASE_SPEED: number = 5;
export var WORKER_BASE_SPEED: number = 40;
export var LARVA_BASE_SPEED: number = 2;

export var AGE_MAX_QUEEN = 81920;
export var AGE_MAX_WORKER = 200;
export var AGE_MAX_LARVA = 10;
export var AGE_MAX_EGG = 0;